/* Table scripts for the SWP391 project version 1*/

CREATE TABLE Users (
  UserID INT PRIMARY KEY AUTO_INCREMENT,
  Fullname VARCHAR(255),
  Email VARCHAR(255),
  Gender BOOLEAN,
  Dob DATE,
  Address VARCHAR(255),
  Username VARCHAR(255),
  Password VARCHAR(255),
  Role INT,
  RegisterDOB DATE
);

CREATE TABLE Category (
  CategoryID INT PRIMARY KEY AUTO_INCREMENT,
  CategoryName TEXT
);

CREATE TABLE CourseOwner (
  CourseOwnerID INT PRIMARY KEY AUTO_INCREMENT,
  UserID INT,
  BeOwnerDOB DATETIME,
  Phone VARCHAR(10),
  Major VARCHAR(100),
  FOREIGN KEY (UserID) REFERENCES Users(UserID)
);

CREATE TABLE Course (
  CourseID INT PRIMARY KEY AUTO_INCREMENT,
  CourseName TEXT,
  CategoryID INT,
  CreateDate DATETIME,
  CoursePrice DOUBLE,
  CourseOwnerID INT,
  Description TEXT,
  Image BLOB,
  Rating INT,
  CourseStatus INT,
  FOREIGN KEY (CourseOwnerID) REFERENCES CourseOwner(CourseOwnerID),
  FOREIGN KEY (CategoryID) REFERENCES Category(CategoryID)
);

CREATE TABLE Enroll (
  UserID INT,
  CourseId INT,
  EnrollDate DATETIME,
  Checkout DOUBLE,
  ProcessStatus BOOLEAN,
  PRIMARY KEY (UserID, CourseId),
  FOREIGN KEY (UserID) REFERENCES Users(UserID),
  FOREIGN KEY (CourseId) REFERENCES Course(CourseID)
);

CREATE TABLE Discount (
  DiscountID INT PRIMARY KEY AUTO_INCREMENT,
  CourseID INT,
  StartDate DATE,
  EndDate DATE,
  DiscountValue DOUBLE,
  FOREIGN KEY (CourseID) REFERENCES Course(CourseID)
);

CREATE TABLE Material (
  MaterialID INT PRIMARY KEY AUTO_INCREMENT,
  CourseID INT,
  Material_link VARCHAR(255),
  MaterialTitle VARCHAR(255),
  MaterialName VARCHAR(255),
  MaterialDOB DATE,
  FOREIGN KEY (CourseID) REFERENCES Course(CourseID)
);

CREATE TABLE CourseSection (
  CourseSectionID INT PRIMARY KEY AUTO_INCREMENT,
  CourseID INT,
  SectionTitle VARCHAR(255),
  SectionDOB DATETIME,
  SectionOrder INT,
  FOREIGN KEY (CourseID) REFERENCES Course(CourseID)
);

CREATE TABLE CourseContent (
  ContentID INT PRIMARY KEY AUTO_INCREMENT,
  CourseSectionID INT,
  ContentTitle VARCHAR(255),
  LinkVideo VARCHAR(255),
  ContentDOB DATE,
  ContentOrder INT,
  FOREIGN KEY (CourseSectionID) REFERENCES CourseSection(CourseSectionID)
);

CREATE TABLE Discuss (
  CommentID INT PRIMARY KEY AUTO_INCREMENT,
  CourseID INT,
  ParentID INT,
  Content TEXT,
  PostDate DATETIME,
  Vote INT,
  FOREIGN KEY (CourseID) REFERENCES Course(CourseID)
);

CREATE TABLE Quiz (
  QuizID INT PRIMARY KEY AUTO_INCREMENT,
  CourseSectionID INT,
  Content TEXT,
  QuizDOB DATE,
  FOREIGN KEY (CourseSectionID) REFERENCES CourseSection(CourseSectionID)
);

CREATE TABLE Question (
  QuestionID INT PRIMARY KEY AUTO_INCREMENT,
  QuizID INT,
  Content TEXT,
  QuestionOrder INT,
  FOREIGN KEY (QuizID) REFERENCES Quiz(QuizID)
);

CREATE TABLE Answer (
  AnswerID INT PRIMARY KEY AUTO_INCREMENT,
  QuestionID INT,
  Content TEXT,
  Correct BOOLEAN,
  AnswerOrder INT,
  FOREIGN KEY (QuestionID) REFERENCES Question(QuestionID)
);

CREATE TABLE FeedbackDetail (
  FeedbackID INT PRIMARY KEY AUTO_INCREMENT,
  UserID INT,
  CourseID INT,
  FeedBackContent TEXT,
  FeedBackDate DATETIME,
  Rating INT,
  FOREIGN KEY (CourseID) REFERENCES Course(CourseID),
  FOREIGN KEY (UserID) REFERENCES Users(UserID)
);

CREATE TABLE Schedule (
  ScheduleID INT PRIMARY KEY AUTO_INCREMENT,
  TitleSchedule VARCHAR(255),
  CourseID INT,
  SetTime DATE,
  ReminderAfter DATE,
  UserID INT,
  FOREIGN KEY (CourseID) REFERENCES Course(CourseID),
  FOREIGN KEY (UserID) REFERENCES Users(UserID)
);

CREATE TABLE CourseProcess (
  UserID INT,
  ContentID INT,
  ContentStatus BOOLEAN,
  PRIMARY KEY (UserID, ContentID),
  FOREIGN KEY (UserID) REFERENCES Users(UserID),
  FOREIGN KEY (ContentID) REFERENCES CourseContent(ContentID)
);

CREATE TABLE MyQuizReport (
  UserID INT,
  QuizID INT,
  Result DOUBLE,
  ReportDOB DATETIME,
  PRIMARY KEY (UserID, QuizID),
  FOREIGN KEY (UserID) REFERENCES Users(UserID),
  FOREIGN KEY (QuizID) REFERENCES Quiz(QuizID)
);

