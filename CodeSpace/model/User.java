import java.util.Date;

public class User {
    private int ID;
    private String username;
    private String password;
    private int role;
    private String fullname;
    private String email;
    private boolean gender;
    private Date dob;
    private String address;
    private Date registerDOB;
    public User(int iD, String username, String password, int role, String fullname, String email, boolean gender,
            Date dob, String address, Date registerDOB) {
        ID = iD;
        this.username = username;
        this.password = password;
        this.role = role;
        this.fullname = fullname;
        this.email = email;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.registerDOB = registerDOB;
    }
    public int getID() {
        return ID;
    }
    public void setID(int iD) {
        ID = iD;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public int getRole() {
        return role;
    }
    public void setRole(int role) {
        this.role = role;
    }
    public String getFullname() {
        return fullname;
    }
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public boolean isGender() {
        return gender;
    }
    public void setGender(boolean gender) {
        this.gender = gender;
    }
    public Date getDob() {
        return dob;
    }
    public void setDob(Date dob) {
        this.dob = dob;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public Date getRegisterDOB() {
        return registerDOB;
    }
    public void setRegisterDOB(Date registerDOB) {
        this.registerDOB = registerDOB;
    }
    
}
