package groupid1.artifactid1.StudentModel;

import java.time.LocalDate;

public class Student {
    String name;
    int age;
    String dob;
    public Student() {
    }
    public Student(String name, int age, String dob) {
        this.name = name;
        this.age = age;
        this.dob = dob;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getDob() {
        return dob;
    }
    public void setDob(String dob) {
        this.dob = dob;
    }
    
    
}
