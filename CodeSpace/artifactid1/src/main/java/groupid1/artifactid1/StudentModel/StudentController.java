package groupid1.artifactid1.StudentModel;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;

public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }

    
    public List<Student> getStudents(){
        return studentService.getStudent();
    }
}
