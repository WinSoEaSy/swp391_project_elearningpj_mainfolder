package groupid1.artifactid1.StudentModel;

import java.time.Month;
import java.util.List;

public class StudentService {
    public List<Student> getStudent(){
        return List.of(
            new Student(
                "Pham Long",
                22,
                "04-09-2003"
            )
        );
    } 
}
