package groupid1.artifactid1;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import groupid1.artifactid1.StudentModel.Student;
import groupid1.artifactid1.StudentModel.StudentController;
import groupid1.artifactid1.StudentModel.StudentService;

@SpringBootApplication
@RestController
public class Artifactid1Application {

	public static void main(String[] args) {
		SpringApplication.run(Artifactid1Application.class, args);
	}

	@GetMapping
	public List<Student> hello(){
		StudentController scontroller = new StudentController(new StudentService());
		return scontroller.getStudents();
	}

}
